interface PatientModel {
    id: string;
    lastName: string;
    firstName: string;
    service: string;
    numberOfRoom: number;
    numberOfBed: number;
    floor: number;
    sex: string;
    stateOfRinsingLiquid: string;
    stateOfUrinaryCollector: string;
    date?: number;
    amountOfUrine?: null;
    hour?: null;
    duration?: number;
    ph?: null;
}
