interface AppareilModel {
    id: string;
    available: string;
    etat: string;
    sterilisation: boolean;
    action: null;
    number: number;
    service: string;
}
