export interface UrineModel {
  date?: string;
  hour?: string;
  volumeUrine?: string;
  volume_TotalUrine?: string;
  id?: string;
  volume_eauUsee?: string;
  volume_Total_eauUsee?: string;
}
