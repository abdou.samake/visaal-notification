import admin from "firebase-admin";
import {QuerySnapshot} from "@google-cloud/firestore";
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import {UrineModel} from "./urine";
import DocumentReference = admin.firestore.DocumentReference;
const app = express();
app.use(cors());
app.use(bodyParser.json());
const db = admin.firestore();
const patientRefs = db.collection('patients')
const refUrines = db.collection('urines');

export async function getAllUrines(): Promise<UrineModel[]> {
    const urineQuerySnap: QuerySnapshot = await refUrines.get();
    const urines: UrineModel[] = [];
    urineQuerySnap.forEach(urineSnap => urines.push(urineSnap.data() as UrineModel));
    return urines;
}

export async function getUrineById(urineId: string): Promise<UrineModel> {
    if (!urineId) {
        throw new Error(`urineId required`);
    }
    const urineToFindRef = refUrines.doc(urineId);
    const urineToFindSnap: DocumentSnapshot = await urineToFindRef.get();
    if (urineToFindSnap.exists) {
        return urineToFindSnap.data() as UrineModel;
    } else {
        throw new Error('object does not exists');
    }
}
export async function postNewUrineData(newUrineData: any, patientId: string): Promise<any> {
    if (!patientId || !newUrineData) {
        throw Error('hotelId and newRoom are missing')
    }
    const refPatient: DocumentReference = patientRefs.doc(patientId);
    const snapShotData: DocumentSnapshot = await refPatient.get();
    if (!snapShotData.exists) {
        return 'hotel dose not existe';
    }
    const createDataUrineRef: DocumentReference = await refUrines.add(newUrineData);
    const createDataUrineInPatient: DocumentReference = refPatient
        .collection('urines')
        .doc(createDataUrineRef.id);
    await createDataUrineInPatient.set({ref: createDataUrineInPatient, id: createDataUrineInPatient.id, ...newUrineData});
    return {...newUrineData, id: createDataUrineInPatient.id};

}
