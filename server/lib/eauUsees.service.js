"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
const db = firebase_admin_1.default.firestore();
const patientRefs = db.collection('patients');
const refEauxUsees = db.collection('eauxUsees');
async function getAllEauxUsees() {
    const eauUseeQuerySnap = await refEauxUsees.get();
    const eauxUsees = [];
    eauUseeQuerySnap.forEach(eauSnap => eauxUsees.push(eauSnap.data()));
    return eauxUsees;
}
exports.getAllEauxUsees = getAllEauxUsees;
async function getEauUseeById(eauUseeId) {
    if (!eauUseeId) {
        throw new Error(`eauUseeId required`);
    }
    const eauUseeToFindRef = refEauxUsees.doc(eauUseeId);
    const eauUseeToFindSnap = await eauUseeToFindRef.get();
    if (eauUseeToFindSnap.exists) {
        return eauUseeToFindSnap.data();
    }
    else {
        throw new Error('object does not exists');
    }
}
exports.getEauUseeById = getEauUseeById;
async function postNewEauUseeData(newEauUseeData, patientId) {
    if (!patientId || !newEauUseeData) {
        throw Error('hotelId and newRoom are missing');
    }
    const refPatient = patientRefs.doc(patientId);
    const snapShotData = await refPatient.get();
    if (!snapShotData.exists) {
        return 'patient dose not existe';
    }
    const createDataEauUseeRef = await refEauxUsees.add(newEauUseeData);
    const createDataEauUseeInPatient = refPatient
        .collection('eauxUsees')
        .doc(createDataEauUseeRef.id);
    await createDataEauUseeInPatient.set(Object.assign({ ref: createDataEauUseeInPatient, id: createDataEauUseeInPatient.id }, newEauUseeData));
    return Object.assign(Object.assign({}, newEauUseeData), { id: newEauUseeData.id });
}
exports.postNewEauUseeData = postNewEauUseeData;
//# sourceMappingURL=eauUsees.service.js.map