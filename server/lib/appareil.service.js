"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
const db = firebase_admin_1.default.firestore();
const refAppareils = db.collection('appareils');
async function getAllAppareils() {
    const appareilQuerySnap = await refAppareils.get();
    const appareils = [];
    appareilQuerySnap.forEach(appareilSnap => appareils.push(appareilSnap.data()));
    return appareils;
}
exports.getAllAppareils = getAllAppareils;
async function getAppareilById(appareilId) {
    if (!appareilId) {
        throw new Error(`appareilId required`);
    }
    const appareilToFindRef = refAppareils.doc(appareilId);
    const appareilToFindSnap = await appareilToFindRef.get();
    if (appareilToFindSnap.exists) {
        return appareilToFindSnap.data();
    }
    else {
        throw new Error('object does not exists');
    }
}
exports.getAppareilById = getAppareilById;
async function testIfAppareilExistsById(appareilId) {
    const appareilRef = refAppareils.doc(appareilId);
    const snapAppareilToFind = await appareilRef.get();
    const appareilToFind = snapAppareilToFind.data();
    if (!appareilToFind) {
        throw new Error(`${appareilId} does not exists`);
    }
    return appareilRef;
}
async function postNewAppareil(newAppareil) {
    if (!newAppareil) {
        throw new Error(`new appareil must be filled`);
    }
    const addResult = await refAppareils.add(newAppareil);
    const createNewAppareil = refAppareils
        .doc(addResult.id);
    await createNewAppareil.set(Object.assign(Object.assign({}, newAppareil), { id: createNewAppareil.id }));
    return Object.assign(Object.assign({}, newAppareil), { id: createNewAppareil.id });
}
exports.postNewAppareil = postNewAppareil;
async function deleteAppareilById(appareilId) {
    if (!appareilId) {
        throw new Error('appareilId is missing');
    }
    const appareilToDeleteRef = await testIfAppareilExistsById(appareilId);
    await appareilToDeleteRef.delete();
    return { response: `${appareilId} -> delete ok` };
}
exports.deleteAppareilById = deleteAppareilById;
async function updateAppareil(appareilId, newAppareil) {
    if (!newAppareil || !appareilId) {
        throw new Error(`appareil data and patient id must be filled`);
    }
    const appareilToPatchRef = await testIfAppareilExistsById(appareilId);
    await appareilToPatchRef.update(newAppareil);
    return newAppareil;
}
exports.updateAppareil = updateAppareil;
//# sourceMappingURL=appareil.service.js.map