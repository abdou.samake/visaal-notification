"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hostelsData = [
    {
        id: 1,
        name: 'hotel rose',
        roomNumbers: 10,
        pool: true,
        rooms: [1, 2, 3, 4, 5, 6, 20, 21, 22, 23, 30],
    },
    {
        id: 2,
        name: 'hotel ocean',
        roomNumbers: 15,
        pool: false,
        rooms: [7, 8, 9, 10, 11, 12, 13, 14, 24, 25, 31],
    },
    {
        id: 3,
        name: 'hotel des Pins',
        roomNumbers: 7,
        pool: true,
        rooms: [15, 16, 17, 18, 19, 26, 27, 28, 29, 32],
    },
];
exports.rooms = [
    {
        roomName: 'suite de luxe',
        size: 2,
        id: 1,
    },
    {
        roomName: 'suite nuptiale',
        size: 2,
        id: 2,
    },
    {
        roomName: 'suite familiale',
        size: 4,
        id: 3,
    },
    {
        roomName: 'suite budget',
        size: 2,
        id: 4,
    },
    {
        roomName: 'suite familiale',
        size: 4,
        id: 5,
    },
    {
        roomName: 'suite budget',
        size: 3,
        id: 6,
    },
    {
        roomName: 'suite de luxe',
        size: 2,
        id: 7,
    },
    {
        roomName: 'suite familiale',
        size: 4,
        id: 8,
    },
    {
        roomName: 'suite de luxe',
        size: 3,
        id: 9,
    },
    {
        roomName: 'suite présidentielle',
        size: 5,
        id: 10,
    },
    {
        roomName: 'suite pacifique',
        size: 2,
        id: 11,
    },
    {
        roomName: 'suite atlantique',
        size: 2,
        id: 12,
    },
    {
        roomName: 'suite manche',
        size: 4,
        id: 13,
    },
    {
        roomName: 'suite mer du nord',
        size: 2,
        id: 14,
    },
    {
        roomName: 'suite pacifique',
        size: 4,
        id: 15,
    },
    {
        roomName: 'suite mer du nord',
        size: 3,
        id: 16,
    },
    {
        roomName: 'suite atlantique',
        size: 2,
        id: 17,
    },
    {
        roomName: 'chambre suite pacifique',
        size: 4,
        id: 18,
    },
    {
        roomName: 'tata suite atlantique',
        size: 3,
        id: 19,
    },
    {
        roomName: 'suite atlantique',
        size: 5,
        id: 20,
    },
    {
        roomName: 'suite pacifique',
        size: 2,
        id: 21,
    },
    {
        roomName: 'suite mer du nord',
        size: 2,
        id: 22,
    },
    {
        roomName: 'suite manche',
        size: 4,
        id: 23,
    },
    {
        roomName: 'suite manche',
        size: 3,
        id: 24,
    },
    {
        roomName: 'suite mer du nord',
        size: 5,
        id: 25,
    },
    {
        roomName: 'suite bordelaise',
        size: 2,
        id: 26,
    },
    {
        roomName: 'suite marseillaise',
        size: 2,
        id: 27,
    },
    {
        roomName: 'suite nicoise',
        size: 4,
        id: 28,
    },
    {
        roomName: 'suite canoise',
        size: 2,
        id: 29,
    },
    {
        roomName: 'suite hendaiar',
        size: 4,
        id: 30,
    },
    {
        roomName: 'suite canoise',
        size: 3,
        id: 31,
    },
    {
        roomName: 'suite nicoise',
        size: 2,
        id: 32,
    },
];
//# sourceMappingURL=hostels.data.js.map