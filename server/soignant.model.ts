export interface SoignantModel {
    lastname: string;
    firstname: string;
    service: string;
    action: null;
    id?: string;
}
