import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AddPatientComponent} from './patients/add-patient/add-patient.component';
import {EditPatientComponent} from './patients/edit-patient/edit-patient.component';
import {ListPatientsComponent} from './patients/list-patients/list-patients.component';
import {HomeComponent} from './home/home.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {ListSoignantComponent} from './soignants/list-soignant/list-soignant.component';
import {AddSoignantComponent} from './soignants/add-soignant/add-soignant.component';
import {ListAppareilComponent} from './appareils/list-appareil/list-appareil.component';
import {AddAppareilComponent} from './appareils/add-appareil/add-appareil.component';
import {HomeDataPatientComponent} from './home-data-patient/home-data-patient.component';
import {DataPatientListComponent} from './patients/data-patient-list/data-patient-list.component';
import {DataPatientDetailsComponent} from './patients/data-patient-details/data-patient-details.component';
import {DetailsPatientsComponent} from './patients/details-patients/details-patients.component';
import {HandleSoignantsComponent} from './soignants/handle-soignants/handle-soignants.component';
import {DetailsSoignantsComponent} from './soignants/details-soignants/details-soignants.component';

const routes: Routes = [
  {path: '', redirectTo: 'sign-up', pathMatch: 'full'},
  {path: 'accueil', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'list-patients', component: ListPatientsComponent},
  {path: 'add-patient', component: AddPatientComponent},
  {path: 'edit-patient', component: EditPatientComponent},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'list-soignant', component: ListSoignantComponent},
  {path: 'add-soignant', component: AddSoignantComponent},
  {path: 'list-appareil', component: ListAppareilComponent},
  {path: 'add-appareil', component: AddAppareilComponent},
  {path: 'data-patients', component: HomeDataPatientComponent},
  {path: 'data-list', component: DataPatientListComponent},
  {path: 'data-details', component: DataPatientDetailsComponent},
  {path: 'details-patients', component: DetailsPatientsComponent},
  {path: 'gerer-soignants', component: HandleSoignantsComponent},
  {path: 'details-soignants', component: DetailsSoignantsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
