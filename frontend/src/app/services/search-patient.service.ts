import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import {PatientModel, PatientRolesEnum} from '../models/patient';
@Injectable({
  providedIn: 'root'
})
export class SearchPatientService {

  constructor(private afs: AngularFirestore) { }
  searchPatient$(nameToSearch: string, role?: PatientRolesEnum): Observable<PatientModel[]> {

    const strSearch = nameToSearch;
    const strLength = strSearch.length;
    const strFrontCode = strSearch.slice(0, strLength - 1);
    const strEndCode = strSearch.slice(strLength - 1, strSearch.length);
    const startCode = strSearch;

    const endCode = strFrontCode + String.fromCharCode(strEndCode.charCodeAt(0) + 1);
    if (role) {
      return this.afs.collection<PatientModel>('users', ref => ref
        .where('lastName', '>=', startCode)
        .where('lastName', '<', endCode)
        .where(role, '==', true),
      )
        .valueChanges();
    } else {
      return this.afs.collection<PatientModel>('patients', ref => ref
        .where('lastName', '>=', startCode)
        .where('lastName', '<', endCode),
      )
        .valueChanges();
    }

  }
}
