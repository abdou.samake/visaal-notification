import { Injectable } from '@angular/core';
import {from, Observable} from 'rxjs';
import {PatientModel} from '../models/patient';
import {tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import {environment} from '../../environments/environment';
import {SoignantModel} from '../models/soignant';
import DocumentReference = firebase.firestore.DocumentReference;
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class SoignantService {

  constructor(private http: HttpClient, private afs: AngularFirestore) { }
  apiRest: string = environment.urlDataBase;
  soignant: SoignantModel;
  /*getAllSoignant$(): Observable<SoignantModel[]>{
    return (this.http.get(this.apiRest + 'soignants') as Observable<SoignantModel[]>);
  }*/
  getAllSoignant$(): Observable<SoignantModel[]> {
    return (this.afs.collection('soignants').valueChanges() as Observable<SoignantModel[]>);
  }
    getSoignantById$(id: string): Observable<SoignantModel>{
    return this.afs.collection('soignants').doc(id).valueChanges() as Observable<SoignantModel>;
  }

  /*addSoignant$(soignant: SoignantModel): Observable<any>{
    return this.http.post(this.apiRest + 'soignants' , soignant);
  }*/
  async addSoignant$(soignant: SoignantModel) {
    const soignantAdd: DocumentReference = await this.afs.collection('soignants').add(soignant);
    const createNewSoignant = this.afs.collection('soignants').doc(soignantAdd.id);
    createNewSoignant.set({...soignant, id: soignantAdd.id});
    return soignant;
  }
  /*deleteSoignant$(id: string): Observable<any>{
    return (this.http.delete(this.apiRest + 'soignants' + '/' + id) as Observable<any>)
      .pipe(
        tap(soignant => this.soignant = soignant)
      );
  }*/
  deleteSoignant$(soignant: SoignantModel): Promise<any>{
    return (this.afs.collection('soignants').doc(soignant.id).delete());
  }
  updateSoignant$(id: string, soignant: {
    lastName: string;
    firstName: string;
    dateOfBirthday: string;
    service: string;
    sex: string
  }): Observable<any>{
    return (this.http.patch(this.apiRest + 'soignants' + '/' + id, soignant) as Observable<any>);
  }

  updateSoignantById$(soignant: SoignantModel): Observable<any> {
    return from(this.afs.collection('soignants').doc(soignant.id)
      .update({...soignant})) as Observable<any>;
  }
}
