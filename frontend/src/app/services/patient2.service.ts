import { Injectable } from '@angular/core';
import {PatientModel} from '../models/patient';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import {Observable} from 'rxjs';
import {DataPatient} from '../models/data-patient';

@Injectable({
  providedIn: 'root'
})
export class Patient2Service {
  private dbPath = '/Sensor';

  TutorialsRef: AngularFireList<any>;
  tutorialsRef: AngularFireObject<any>;

  constructor(private db: AngularFireDatabase) {
    this.TutorialsRef = db.list(this.dbPath);
  }

  getAll(): AngularFireList<any> {
    return this.TutorialsRef;
  }

  /*create(patient: PatientModel): any {
    return this.TutorialsRef.push(patient);
  }

  update(id: string, value: any): Promise<void> {
    return this.TutorialsRef.update(id, value);
  }

  delete(id): any{
    // return this.tutorialsRef.remove(id);
   this.tutorialsRef = this.db.object(this.dbPath + '/' + id);
   return this.tutorialsRef.remove();
  }

  deleteAll(): Promise<void> {
    return this.tutorialsRef.remove();
  }
  /*DeleteStudent(id: string) {
    this.tutorialsRef = this.db.object(this.dbPath + id);
    this.tutorialsRef.remove();
  }*/
}
