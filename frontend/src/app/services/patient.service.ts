import { Injectable } from '@angular/core';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {filter, tap} from 'rxjs/operators';
import {PatientModel} from '../models/patient';
import { AngularFirestore } from '@angular/fire/firestore';
import DocumentReference = firebase.firestore.DocumentReference;
import * as firebase from 'firebase';
import {UrinesModel} from '../models/urines';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  patient$: BehaviorSubject<PatientModel> = new BehaviorSubject<PatientModel>(null);
  patient: PatientModel;
  constructor(private http: HttpClient, private afs: AngularFirestore) { }

  apiRest: string = environment.urlDataBase;

  getPatient$(): Observable<PatientModel> {
    return this.patient$
      .pipe(
        filter(patient => !!patient)
      );
  }

  /*getAllPatient$(): Observable<PatientModel[]>{
    return (this.http.get(this.apiRest + 'patients') as Observable<PatientModel[]>);
  }*/
 getAllPatient$(): Observable<PatientModel[]> {
    return this.afs.collection('patients').valueChanges() as Observable<PatientModel[]>;
}
 /* getPatientById$(id: string): Observable<PatientModel>{
    return this.http.get(this.apiRest + 'patients' + '/' + id) as Observable<PatientModel>;
  }*/
  getPatientById$(id: string): Observable<PatientModel>{
    return this.afs.collection('patients').doc(id).valueChanges() as Observable<PatientModel>;
  }
  /*addPatient$(patient: PatientModel): Observable<PatientModel>{
    return this.http.post(this.apiRest + 'patients' , patient) as Observable<PatientModel>;
  }*/
  // tslint:disable-next-line:typedef
  async addPatient$(patient: PatientModel) {
    const patientAdd: DocumentReference = await this.afs.collection('patients').add(patient);
    const createNewPatient = this.afs.collection('patients').doc(patientAdd.id);
    createNewPatient.set({...patient, id: patientAdd.id});
    return patient;
  }
  /*deletePatient$(id: string): Observable<any>{
    return (this.http.delete(this.apiRest + 'patients' + '/' + id) as Observable<any>)
      .pipe(
        tap(patient => this.patient = patient)
      );
  }*/

  deletePatient$(patient: PatientModel): Promise<any>{
    return (this.afs.collection('patients').doc(patient.id).delete());
  }

  updatePatient$(id: string, patient: {
    lastName: string;
    firstName: string;
    numberOfBed: number;
    dateOfBirthday: number;
    service: string;
    sex: string;
    numberOfRoom: number;
    numberOfDevice: number
  }): Observable<any>{
    return (this.http.patch(this.apiRest + 'patients' + '/' + id, patient) as Observable<any>);
  }
  /*findPatientByName$(patient: PatientModel): Observable<any>{
    return this.http.get(this.apiRest + 'patients' + '/' + patient.lastName);
  }*/
 updatePatientById$(patient: PatientModel): Observable<any> {
    return from(this.afs.collection('patients').doc(patient.id)
      .update({...patient})) as Observable<any>;
  }
  /*getPatientById(id: string): Observable<any> {
    return this.http.get(this.apiRest + 'patients' + '/' + id) as Observable<any>;
  }*/
  /*getUrineById(id: string): Observable<any> {
    return this.http.get(this.apiRest + 'urines' + '/' + id) as Observable<any>;
  }*/
  getAllUrine(): Observable<any> {
    return this.http.get(this.apiRest + 'urines') as Observable<any>;
  }
  getUrineById(id: string): Observable<UrinesModel> {
    return this.afs.collection('urines').doc(id).valueChanges() as Observable<UrinesModel>;
  }
  }
