import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {PatientModel} from '../models/patient';
import {filter} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import {AppareilModel} from '../models/appareil';

@Injectable({
  providedIn: 'root'
})
export class AppareilService {

  constructor(private http: HttpClient, private afs: AngularFirestore) { }
  apiRest: string = environment.urlDataBase;
  getAllAppareils$(): Observable<AppareilModel[]>{
    return (this.http.get(this.apiRest + 'appareils') as Observable<AppareilModel[]>);
  }

  addAppareil$(appareil: AppareilModel): Observable<any>{
    return this.http.post(this.apiRest + 'appareils' , appareil);
  }
}
