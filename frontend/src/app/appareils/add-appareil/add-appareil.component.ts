import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PatientService} from '../../services/patient.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from '../../services/auth.service';
import {AppareilService} from '../../services/appareil.service';

@Component({
  selector: 'app-add-appareil',
  templateUrl: './add-appareil.component.html',
  styleUrls: ['./add-appareil.component.scss']
})
export class AddAppareilComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private appareilService: AppareilService,
              private afAuth: AngularFireAuth,
              public authService: AuthService
  ) {
  }

  addForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      available: ['', Validators.required],
      state: ['', Validators.required],
      sterilisation: ['', Validators.required],
      action: ['', Validators.required],
      service: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.addForm.valid) {
      this.appareilService.addAppareil$(this.addForm.value)
        .subscribe(data => {
          console.log(data);
          this.router.navigate(['/list-appareil']);
        });
    }
  }
  // tslint:disable-next-line:typedef
  get f() { return this.addForm.controls; }

  disco(): void
  {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
  ListePatient(): void {
    this.router.navigate(['list-patients']);
  }
listAppareil() {
  this.router.navigate(['list-appareil']);
}

}
