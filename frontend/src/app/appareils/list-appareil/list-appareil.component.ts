import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {AppareilService} from '../../services/appareil.service';
import {AppareilModel} from '../../models/appareil';

@Component({
  selector: 'app-list-appareil',
  templateUrl: './list-appareil.component.html',
  styleUrls: ['./list-appareil.component.scss']
})
export class ListAppareilComponent implements OnInit {

  filterTerm: string;
  theme = true;

  // tslint:disable-next-line:max-line-length
  constructor(public authService: AuthService,
              private router: Router,
              private appareilService: AppareilService,
              private afAuth: AngularFireAuth) {
  }

  appareils: AppareilModel[];
  appareil: AppareilModel;

  logout(): void {
    this.authService.signOut();
  }

  ngOnInit(): void {
    this.getAllAppareils();
  }

  getAllAppareils(): void {
    this.appareilService.getAllAppareils$().subscribe(data => {
      this.appareils = data;
    });
  }

  addPatient(): void {
    this.router.navigate(['add-patient']);
  }


  updatePatient():
    void {
    this.router.navigateByUrl('edit-patient');
  }

  // tslint:disable-next-line:typedef
  seDeconnecter()
  {
    this.authService.deconnecter();
    /*this.router.navigateByUrl('/login');*/
  }

  // tslint:disable-next-line:typedef
  disco()
  {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
  addAppareil() {
    this.router.navigate(['add-appareil']);
  }

}
