import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {Router} from '@angular/router';
import {PatientModel} from '../../models/patient';
import {PatientService} from '../../services/patient.service';
import {Patient2Service} from '../../services/patient2.service';
import {map} from 'rxjs/operators';
import {DataPatient} from '../../models/data-patient';



@Component({
  selector: 'app-list-patients',
  templateUrl: './list-patients.component.html',
  styleUrls: ['./list-patients.component.scss']
})
export class ListPatientsComponent implements OnInit {
  filterTerm: string;
  theme = true;
  message = '';
  currentPatient = null;
  currentIndex = -1;
  // tslint:disable-next-line:max-line-length
  constructor(public authService: AuthService,
              private router: Router,
              private patientsService: PatientService,
              private afAuth: AngularFireAuth,
              private patient2Service: Patient2Service) {
  }

  patients: PatientModel[];
  patient: PatientModel;

  logout(): void {
    this.authService.signOut();
  }
  refreshList(): void {
    this.currentPatient = null;
    this.currentIndex = -1;
    this.getAllPatients();
  }
  setActiveTutorial(patient, index): void {
    this.currentPatient = patient;
    this.currentIndex = index;
  }
  ngOnInit(): void {
    this.getAllPatients();
  }

  getAllPatients(): void {
    this.patientsService.getAllPatient$().subscribe(data => {
      this.patients = data;
    });
  }
/*getAll(): void {
  this.patient2Service.getAll().snapshotChanges().pipe(
    map(changes =>
      changes.map(c =>
        ({ key: c.payload.key, ...c.payload.val() })
      )
    )
  ).subscribe(data => {
    this.patients = data;
  });
}*/

  addPatient(): void {
    this.router.navigate(['add-patient']);
  }

  /*deletePatient(patient: PatientModel): void {
    this.patient2Service.delete(patient.id);
      // this.refreshList.emit();
      // this.message = 'The tutorial was updated successfully!';
  }*/
  deletePatient(patient: PatientModel): void {
    this.patientsService.deletePatient$(patient).catch(err => console.log(err));
      /*.subscribe(data => {
        console.log(data);
        this.getAllPatients();
      })*/
  }

  showPatient(patientId: string): void{
    this.router.navigateByUrl('affich-patient');
    this.patientsService.getPatientById$(patientId)
      .subscribe(data => this.patient = data);
    this.patientsService.getAllPatient$().subscribe(data => {
      this.patients = data.map(patient => this.patient = patient);
    });
  }
    updatePatient():
    void {
      this.router.navigateByUrl('edit-patient');
    }
  datPatient():
    void {
    this.router.navigateByUrl('data-patients');
  }
  accueil():
    void {
    this.router.navigateByUrl('accueil');
  }

    // tslint:disable-next-line:typedef
    seDeconnecter()
    {
      this.authService.deconnecter();
      /*this.router.navigateByUrl('/login');*/
    }

    // tslint:disable-next-line:typedef
    disco()
    {
      this.afAuth.auth.signOut();
      // @ts-ignore
      this.router.navigateByUrl(['']);
    }
  }

