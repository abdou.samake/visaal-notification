import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PatientService} from '../../services/patient.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from '../../services/auth.service';
import {Patient2Service} from '../../services/patient2.service';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.scss']
})
export class AddPatientComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private patientsService: PatientService,
              private patientService: Patient2Service,
              private afAuth: AngularFireAuth,
              public authService: AuthService
              ) {
  }

  addForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      dateOfBirthday: ['', Validators.required],
      sex: ['', Validators.required],
      service: ['', Validators.required],
      floor: ['', [Validators.required]],
      numberOfRoom: ['', Validators.required],
      numberOfBed: ['', Validators.required],
      numberOfDevice: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.addForm.valid) {
      this.patientsService.addPatient$(this.addForm.value)
        .catch(err => console.log(err));
      this.router.navigate(['/list-patients']);
        /*.subscribe(
        data => {
        console.log(data);
        this.submitted = true;
        this.router.navigate(['/list-patients']);
      });*/
        /*.subscribe(data => {
          console.log(data);
          this.router.navigate(['/list-patients']);
        });*/
    }
  }
  // tslint:disable-next-line:typedef
  get f() { return this.addForm.controls; }

  accueil():
    void {
    this.router.navigateByUrl('accueil');
  }
  disco(): void
  {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
  ListePatient(): void {
    this.router.navigate(['list-patients']);
  }
  updatePatient():
    void {
    this.router.navigateByUrl('edit-patient');
  }
}
