import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PatientModel} from '../../models/patient';
import {PatientService} from '../../services/patient.service';
import {switchMap, tap} from 'rxjs/operators';
import {combineLatest} from 'rxjs';
import {UrinesModel} from '../../models/urines';
import {EauUseeModele} from '../../models/eau-usees';

@Component({
  selector: 'app-details-patients',
  templateUrl: './details-patients.component.html',
  styleUrls: ['./details-patients.component.scss']
})
export class DetailsPatientsComponent implements OnInit {

  @Input() patient: PatientModel;
  @Output() refreshList: EventEmitter<any> = new EventEmitter();
  currentPatient: PatientModel = null;
  message = '';
  urine: UrinesModel;
  urines: UrinesModel[];
  eauUsee: EauUseeModele;

  constructor(private patientService: PatientService) { }
  ngOnInit(): void {
    this.message = '';
  }
  ngOnChanges(): void {
    this.message = '';
    this.currentPatient = { ...this.patient };
  }

  /*updatePublished(status): void {
    this.patientService.updatePatient$(this.currentTutorial.id, { published: status })
      .then(() => {
        this.currentTutorial.lastName = status;
        this.message = 'The status was updated successfully!';
      })
      .catch(err => console.log(err));
  }*/

  updateTutorial(): void {
    const data = {
      lastName: this.currentPatient.lastName,
      firstName: this.currentPatient.firstName,
      dateOfBirthday: this.currentPatient.dateOfBirthday,
      sex: this.currentPatient.sex,
      service: this.currentPatient.service,
      numberOfRoom: this.currentPatient.numberOfRoom,
      numberOfDevice: this.currentPatient.numberOfDevice,
      numberOfBed: this.currentPatient.numberOfBed,
    };

    this.patientService.updatePatientById$(this.currentPatient)
      .subscribe(() => this.message = 'mise à jour réussi!');
  }
  getUrineInPatientById(): void {
    this.patientService.getPatientById$(this.patient.id)
      .pipe(
        tap((data: PatientModel) => this.patient = data),
        switchMap(
          (patient: PatientModel) => combineLatest(patient.urines.map((urineId) => this.patientService
            .getUrineById(urineId)))
        ),
        tap((urines: UrinesModel[]) => this.patient.urinesData = urines)
      ).subscribe();
  }

}
