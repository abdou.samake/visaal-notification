import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PatientService} from '../../services/patient.service';
import {PatientModel} from '../../models/patient';
import {map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-edit-patient',
  templateUrl: './edit-patient.component.html',
  styleUrls: ['./edit-patient.component.scss']
})
export class EditPatientComponent implements OnInit, OnDestroy {

  // @ts-ignore
  patients: PatientModel[] = this.getAllPatients();
  // @ts-ignore
  patient: PatientModel;
  editForm: FormGroup;
  submitted = false;
  currentPatient: PatientModel = null;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private patientService: PatientService,
              private activatedRoute: ActivatedRoute,
              private afAuth: AngularFireAuth,
              public authService: AuthService) {
  }

  getAllPatients(): void {
    this.patientService.getAllPatient$().subscribe(data => {
      this.patients = data;
    });
  }
  getPatientById(): void {
    this.patientService.getPatientById$(this.patient.id).subscribe(x => console.log(x));
  }
testPatient(): void {
    this.patientService.getAllPatient$().subscribe(
      data => {
        this.patients = data.map(patient => patient);
      }
    );
}
  // @ts-ignore
  ngOnInit(): void {

    this.patientService.getAllPatient$().subscribe(data => {
      this.patients = data.map(patient => {
        return {
          id: patient.id, ...patient
        } as PatientModel;
      });
    });
    this.editForm = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      dateOfBirthday: ['', Validators.required],
      sex: ['', Validators.required],
      service: ['', Validators.required],
      floor: ['', Validators.required],
      numberOfRoom: ['', Validators.required],
      numberOfDevice: ['', Validators.required],
    });
  }

  // get the form short name to access the form fields
  // tslint:disable-next-line:typedef
  get f() {
    return this.editForm.controls;
  }
  listePatient(): void {
    this.router.navigate(['list-patients']);
  }

  addPatient(): void {
    this.router.navigate(['add-patient']);
  }

  updatePatientValid(patient: PatientModel): void {
    this.patient = {...this.patient, ...this.editForm.value};
    this.patientService.updatePatient$(patient.id, this.editForm.value)
      .subscribe(data => {
        this.patients = data;
        console.log(data);
        this.router.navigate(['list-patients']);
      });
  }
  // tslint:disable-next-line:typedef
  returnId() {
    return console.log(this.patient.id);
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  disco(): void
  {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
}

