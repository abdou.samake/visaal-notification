import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataPatient} from '../../models/data-patient';
import {DataPatientsService} from '../../services/data-patients.service';

@Component({
  selector: 'app-data-patient-details',
  templateUrl: './data-patient-details.component.html',
  styleUrls: ['./data-patient-details.component.scss']
})
export class DataPatientDetailsComponent implements OnInit {

  @Input() dataPatient: DataPatient;
  @Output() refreshList: EventEmitter<any> = new EventEmitter();
  currentdata: DataPatient = null;
  message = '';

  constructor(private dataPatientsService: DataPatientsService) { }

  ngOnInit(): void {
    this.message = '';
  }

  ngOnChanges(): void {
    this.message = '';
    this.currentdata = { ...this.dataPatient };
  }

  updatePublished(status): void {
    this.dataPatientsService.update(this.currentdata.key, { published: status })
      .then(() => {
        this.currentdata.Date = status;
        this.message = 'The status was updated successfully!';
      })
      .catch(err => console.log(err));
  }

  updateTutorial(): void {
    const data = {
      date: this.currentdata.Date,
      hour: this.currentdata.Heure,
      volum: this.currentdata.Volume,
      fullVolum: this.currentdata.Volume_Totale,
    };

    this.dataPatientsService.update(this.currentdata.key, data)
      .then(() => this.message = 'The tutorial was updated successfully!')
      .catch(err => console.log(err));
  }

  deleteTutorial(): void {
    this.dataPatientsService.delete(this.currentdata.key)
      .then(() => {
        this.refreshList.emit();
        this.message = 'The tutorial was delete successfully!';
      })
      .catch(err => console.log(err));
  }
}
