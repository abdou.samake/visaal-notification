import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  isNewUser = true;
  password = '';
  errorMessage = '';
  error: {name: string, message: string} = {name: '', message: ''};
  authState: any = null;
  email: string;
  mdp: string;
  // tslint:disable-next-line:max-line-length
  constructor(private afAuth: AngularFireAuth, private sanitizer: DomSanitizer, private router: Router, public authService: AuthService) { }
  ngOnInit(): void {
  }
  clearErrorMessage(): void {
    this.errorMessage = '';
    this.error = {name: '', message: ''};
  }

  changeForm(): void {
    this.isNewUser = !this.isNewUser;
  }
  // tslint:disable-next-line:typedef
  signUpWithEmail(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user;
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  }
  onSignUp(): void {
    // tslint:disable-next-line:no-unused-expression
    this.clearErrorMessage();
    // tslint:disable-next-line:no-unused-expression
    if (this.email, this.mdp) {
      this.authService.signUpWithEmail(this.email, this.mdp)
        .then(() => {
          this.router.navigate(['/accueil']);
          // tslint:disable-next-line:variable-name
        }).catch(_error => {
          alert(`l'adresse email existe déja, veuillez vous conncetez!`);
          this.router.navigate(['/sign-up']);
      });
    }
  }
  focusoutHandler(value): void{
    this.email = value;
  }
  focusoutHandler2(value): void{
    this.mdp = value;
  }

}
