import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
// @ts-ignore
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isNewUser = true;
  password = '';
  errorMessage = '';
  error: {name: string, message: string} = {name: '', message: ''};
  authState: any = null;
  email: string;
  mdp: string;
  constructor(
    private afAuth: AngularFireAuth,
    private sanitizer: DomSanitizer,
    private router: Router,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  focusoutHandler(value): void{
    this.email = value;
  }
  focusoutHandler2(value): void{
    this.mdp = value;
  }
  validateForm(email: string, mdp: string): boolean {
    if (email.length === 0) {
      this.errorMessage = 'Please enter Email!';
      return false;
    }
  }
  onLogin(): void{
    console.log(this.email, this.mdp);
    this.afAuth.auth.setPersistence(firebase.auth.Auth.Persistence.SESSION)
      .then(() => this.signMethod())
      // tslint:disable-next-line:only-arrow-functions typedef
      .catch(function(error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
          alert(errorMessage);
        }
      });
  }
  clearErrorMessage(): void {
    this.errorMessage = '';
    this.error = {name: '', message: ''};
  }
  // tslint:disable-next-line:typedef
  login(){
    this.clearErrorMessage();
    if (this.validateForm(this.email, this.password)) {
      this.authService.loginWithEmail(this.email, this.password)
        .then(() => this.router.navigate(['/accueil']))
        // tslint:disable-next-line:variable-name
        .catch(_error => {
          this.error = _error;
          this.router.navigate(['']);
        });
    }
  }
  signMethod(): void{
    // tslint:disable-next-line:only-arrow-functions typedef
    this.afAuth.auth.signInWithEmailAndPassword(this.email, this.mdp).catch(function(error) {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // [START_EXCLUDE]
      if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
        this.router.navigateByUrl('');
      } else {
        alert(errorMessage);
      }
      console.log(error);
      // [END_EXCLUDE]
    }).then(() => this.test2());
  }

    // tslint:disable-next-line:only-arrow-functions typedef
  // tslint:disable-next-line:typedef
  test2(){

    const go = () => this.router.navigateByUrl('/accueil');
    // tslint:disable-next-line:only-arrow-functions typedef
    this.afAuth.auth.onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        const displayName = user.displayName;
        const email = user.email;
        const emailVerified = user.emailVerified;
        console.log(displayName, emailVerified, email);
        go();
        // ...
      } else {
        // User is signed out.
        console.log( 'pas d\'user');
        // ...
      }
    });
  }

  sendPasswordReset(mailAddr): void {
    const email = mailAddr;
    // [START sendpasswordemail]
    // tslint:disable-next-line:only-arrow-functions typedef
    firebase.auth().sendPasswordResetEmail(email).then(function() {
      // Password Reset Email Sent!
      // [START_EXCLUDE]
      alert('email envoyé ');
      // [END_EXCLUDE]
      // tslint:disable-next-line:only-arrow-functions typedef
    }).catch(function(error) {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // [START_EXCLUDE]
      // tslint:disable-next-line:triple-equals
      if (errorCode == 'auth/invalid-email') {
        alert('Adresse email invalide');
        // tslint:disable-next-line:triple-equals
      } else if (errorCode == 'auth/user-not-found') {
        alert('Adresse email invalide');
      }
      console.log(error);
      // [END_EXCLUDE]
    });
    // [END sendpasswordemail];
  }

  forgot(): void {
    const mail = prompt('Entrez votre adresse mail');
    if (mail != null ) {
      if (mail.length){
        this.sendPasswordReset(mail);
      }
    }
  }

  createUser(): void {

    // tslint:disable-next-line:typedef
    this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.mdp).catch(function(error) {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // ...
      if (errorCode ){
        alert(errorCode);
      } else{
        this.router.navigate(['/accueil']);
      }
    });

    }
    /***********************************************************************/

}

