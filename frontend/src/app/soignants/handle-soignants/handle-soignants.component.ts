import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {PatientService} from '../../services/patient.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {Patient2Service} from '../../services/patient2.service';
import {PatientModel} from '../../models/patient';
import {SoignantModel} from '../../models/soignant';
import {SoignantService} from '../../services/soignant.service';

@Component({
  selector: 'app-handle-soignants',
  templateUrl: './handle-soignants.component.html',
  styleUrls: ['./handle-soignants.component.scss']
})
export class HandleSoignantsComponent implements OnInit {
  filterTerm: string;
  theme = true;
  message = '';
  currentSoignant = null;
  currentIndex = -1;
  soignants: SoignantModel[];
  soignant: SoignantModel;
  patients: PatientModel[];
  patient: PatientModel;
  // tslint:disable-next-line:max-line-length
  constructor(public authService: AuthService,
              private router: Router,
              private patientsService: PatientService,
              private afAuth: AngularFireAuth,
              private soignantService: SoignantService) {
  }
  logout(): void {
    this.authService.signOut();
  }
  refreshList(): void {
    this.currentSoignant = null;
    this.currentIndex = -1;
    this.getAllSoignants();
  }
  setActiveTutorial(patient, index): void {
    this.currentSoignant = patient;
    this.currentIndex = index;
  }
  ngOnInit(): void {
    this.getAllSoignants();
  }
  getAllSoignants(): void {
    this.soignantService.getAllSoignant$().subscribe(data => {
      this.soignants = data;
    });
  }
  getAllPatients(): void {
    this.patientsService.getAllPatient$().subscribe(data => {
      this.patients = data;
    });
  }

    addPatient(): void {
    this.router.navigate(['add-patient']);
  }

  addSoignant(): void {
    this.router.navigate(['add-soignant']);
  }
  deletePatient(patient: PatientModel): void {
    this.patientsService.deletePatient$(patient).catch(err => console.log(err));
      /*.subscribe(data => {
        console.log(data);
        this.getAllPatients();
      });*/
  }

  showPatient(patientId: string): void{
    this.router.navigateByUrl('affich-patient');
    this.patientsService.getPatientById$(patientId)
      .subscribe(data => this.patient = data);
    this.patientsService.getAllPatient$().subscribe(data => {
      this.patients = data.map(patient => this.patient = patient);
    });
  }
  updatePatient():
    void {
    this.router.navigateByUrl('edit-patient');
  }
  dataPatient():
    void {
    this.router.navigateByUrl('data-patients');
  }
  accueil():
    void {
    this.router.navigateByUrl('accueil');
  }

  // tslint:disable-next-line:typedef
  seDeconnecter()
  {
    this.authService.deconnecter();
    /*this.router.navigateByUrl('/login');*/
  }

  // tslint:disable-next-line:typedef
  disco()
  {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
}
