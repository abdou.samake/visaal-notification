import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SoignantService} from '../../services/soignant.service';
import {AuthService} from '../../services/auth.service';
import {AngularFireAuth} from 'angularfire2/auth';

@Component({
  selector: 'app-add-soignant',
  templateUrl: './add-soignant.component.html',
  styleUrls: ['./add-soignant.component.scss']
})
export class AddSoignantComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private soignantService: SoignantService,
              public authService: AuthService,
              private afAuth: AngularFireAuth
              ) {
  }

  addForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      sex: ['', Validators.required],
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      service: ['', Validators.required],
      dateOfBirthday: ['', [Validators.required]],
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.addForm.valid) {
      this.soignantService.addSoignant$(this.addForm.value)
        .catch(err => console.log(err));
      this.router.navigate(['/list-soignant']);
        /*.subscribe(data => {
          console.log(data);
          this.router.navigate(['/list-soignant']);
        });*/
    }
  }
  // tslint:disable-next-line:typedef
  get f() { return this.addForm.controls; }
  disco(): void
  {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
  listePatient(): void {
    this.router.navigate(['list-patients']);
  }
  listeSoignant(): void {
    this.router.navigate(['list-soignant']);
  }
  addPatient(): void {
    this.router.navigate(['add-patient']);
  }
}
