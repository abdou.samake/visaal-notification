import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSoignantComponent } from './edit-soignant.component';

describe('EditSoignantComponent', () => {
  let component: EditSoignantComponent;
  let fixture: ComponentFixture<EditSoignantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSoignantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSoignantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
