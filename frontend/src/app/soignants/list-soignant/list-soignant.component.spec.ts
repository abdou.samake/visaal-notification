import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSoignantComponent } from './list-soignant.component';

describe('ListSoignantComponent', () => {
  let component: ListSoignantComponent;
  let fixture: ComponentFixture<ListSoignantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSoignantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSoignantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
