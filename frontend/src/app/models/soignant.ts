export interface SoignantModel {
  lastName: string;
  firstName: string;
  service: string;
  action: null;
  id?: string;
  dateOfBirthday?: string;
  sex: string;
}
