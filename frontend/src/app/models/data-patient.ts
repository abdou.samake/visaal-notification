export interface DataPatient {
  Date: string;
  Heure: string;
  Volume: number;
  Volume_Totale: number;
  key: string;
  name: string;
}
