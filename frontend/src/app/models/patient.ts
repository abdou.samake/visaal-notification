import {UrinesModel} from './urines';

export interface PatientModel {
  id?: string;
  lastName: string;
  firstName: string;
  service: string;
  numberOfRoom: number;
  numberOfBed: number;
  floor: number;
  sex: string;
  stateOfRinsingLiquid: string;
  stateOfUrinaryCollector: string;
  dateOfBirthday: number;
  numberOfDevice: number;
  date: number;
  amountOfUrine: null;
  hour: null;
  duration: number;
  ph: null;
  urines: string[];
  urinesData: UrinesModel[];
}

export enum PatientRolesEnum {
  isPatient = 'isPatient'
}
