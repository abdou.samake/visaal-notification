import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListPatientsComponent } from './patients/list-patients/list-patients.component';
import { AddPatientComponent } from './patients/add-patient/add-patient.component';
import { EditPatientComponent } from './patients/edit-patient/edit-patient.component';
import { AddSoignantComponent } from './soignants/add-soignant/add-soignant.component';
import { EditSoignantComponent } from './soignants/edit-soignant/edit-soignant.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './services/auth.service';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {NzGridModule} from 'ng-zorro-antd/grid';
import {NzFormModule} from 'ng-zorro-antd/form';
import { NotificationsComponent } from './notifications/notifications.component';
import {MatPseudoCheckboxModule} from '@angular/material/core';
import { ListSoignantComponent } from './soignants/list-soignant/list-soignant.component';
import { ListAppareilComponent } from './appareils/list-appareil/list-appareil.component';
import { AddAppareilComponent } from './appareils/add-appareil/add-appareil.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import { DataPatientListComponent } from './patients/data-patient-list/data-patient-list.component';
import { DataPatientDetailsComponent } from './patients/data-patient-details/data-patient-details.component';
import { HomeDataPatientComponent } from './home-data-patient/home-data-patient.component';
import { DetailsPatientsComponent } from './patients/details-patients/details-patients.component';
import { HandleSoignantsComponent } from './soignants/handle-soignants/handle-soignants.component';
import { DetailsSoignantsComponent } from './soignants/details-soignants/details-soignants.component';
import {AsyncPipe} from '@angular/common';
import {MessagingService} from './services/messaging.service';

@NgModule({
  declarations: [
    AppComponent,
    ListPatientsComponent,
    AddPatientComponent,
    EditPatientComponent,
    AddSoignantComponent,
    EditSoignantComponent,
    LoginComponent,
    HomeComponent,
    SignUpComponent,
    NotificationsComponent,
    ListSoignantComponent,
    ListAppareilComponent,
    AddAppareilComponent,
    DataPatientListComponent,
    DataPatientDetailsComponent,
    HomeDataPatientComponent,
    DetailsPatientsComponent,
    HandleSoignantsComponent,
    DetailsSoignantsComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebase),
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        FormsModule,
        AngularFireAuthModule,
        HttpClientModule,
        ReactiveFormsModule,
        Ng2SearchPipeModule,
        AngularFirestoreModule,
        NzGridModule,
        NzFormModule,
        MatPseudoCheckboxModule,
      AngularFireDatabaseModule,
      AngularFireMessagingModule
    ],
  providers: [AuthService, MessagingService, AsyncPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
