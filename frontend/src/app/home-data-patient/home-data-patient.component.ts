import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-data-patient',
  templateUrl: './home-data-patient.component.html',
  styleUrls: ['./home-data-patient.component.scss']
})
export class HomeDataPatientComponent implements OnInit {

  constructor(public authService: AuthService,
              private afAuth: AngularFireAuth,
              private router: Router) { }

  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  disco()
  {
    this.afAuth.auth.signOut();
    this.router.navigate(['']);
  }
  accueil():
    void {
    this.router.navigateByUrl('accueil');
  }
}
