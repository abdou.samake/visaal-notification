import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {PatientService} from '../services/patient.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {PatientModel} from '../models/patient';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  filterTerm: string;
  theme = true;

  // tslint:disable-next-line:max-line-length
  constructor(public authService: AuthService,
              private router: Router,
              private patientsService: PatientService,
              private afAuth: AngularFireAuth) {
  }

  patients: PatientModel[];
  patient: PatientModel;

  logout(): void {
    this.authService.signOut();
  }

  ngOnInit(): void {
    this.getAllPatients();
  }

  getAllPatients(): void {
    this.patientsService.getAllPatient$().subscribe(data => {
      this.patients = data;
    });
  }

  addPatient(): void {
    this.router.navigate(['add-patient']);
  }
  addSoignant(): void {
    this.router.navigate(['add-soignant']);
  }

  deletePatient(patient: PatientModel): void {
    this.patientsService.deletePatient$(patient).catch(err => console.log(err));
      /*.subscribe(data => {
        console.log(data);
        this.getAllPatients();
      })*/
  }

  showPatient(patientId: string): void {
    this.router.navigateByUrl('affich-patient');
    this.patientsService.getPatientById$(patientId)
      .subscribe(data => this.patient = data);
    this.patientsService.getAllPatient$().subscribe(data => {
      this.patients = data.map(patient => this.patient = patient);
    });
  }
  updatePatient():
    void {
    this.router.navigateByUrl('edit-patient');
  }

  // tslint:disable-next-line:typedef
  seDeconnecter()
  {
    this.authService.deconnecter();
    /*this.router.navigateByUrl('/login');*/
  }

  // tslint:disable-next-line:typedef
  disco()
  {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
  listePatient(): void {
    this.router.navigate(['list-soignant']);
  }
  addAppareil() {
    this.router.navigateByUrl('add-appareil');
  }

}
